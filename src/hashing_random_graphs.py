import networkx as nx
import numpy as np
import matplotlib.pyplot as P
import itertools
from collections import defaultdict
from scipy.signal import correlate
from scipy.misc import comb
from sets import Set
from  euclidean_maximal_subgraph_isomorphism import *
import sys
import time
from random_graph_generator import *

def set_random_unit_vectors(number_of_sets, number_of_vectors, dim):
  vector_sets = []
  for i in range(number_of_sets):
    vector_set = []
    for j in range(number_of_vectors):
      vector_set.append(np.random.normal(size=dim))
    vector_sets.append(vector_set)
  return vector_sets

def hash_bit(adjacency_tensor, vector):
  return (np.sign(np.dot(adjacency_tensor, vector)) > 0)

def locality_sensitive_hashing(graph_label_and_fft_array, vectors):
  # labels_and_digraphs are tuples (label, digraph)
  hash_value_to_graph_label = defaultdict(list)
  for label, fft_vector in graph_label_and_fft_array:
    hash_value = 0
    for i in range(len(vectors)):
      vector = vectors[i]
      hash_value += np.power(2,i)*hash_bit(fft_vector, vector)
    hash_value_to_graph_label[hash_value].append(label)
  return hash_value_to_graph_label
    
def all_pair_wise_fast(graph_label_and_delta_to_start_tensor_dicts, list_of_digraphs, grid_size, d):
  matchings = defaultdict(list)
  number_of_graphs = len(graph_label_and_delta_to_start_tensor_dicts)
  for i in range(number_of_graphs):
    label1, delta_to_start_tensor1 = graph_label_and_delta_to_start_tensor_dicts[i]
    for j in range(i+1, number_of_graphs):
      label2, delta_to_start_tensor2 = graph_label_and_delta_to_start_tensor_dicts[j]
      num_matches = fast_edge_maximal_isomorphism(delta_to_start_tensor1, delta_to_start_tensor2, list_of_all_deltas, grid_size, d)
      matchings[(label1, label2)] = num_matches
  return matchings

def pair_wise_brute_force(dg1, dg2, print_matches=False):
  matched_edges = brute_force_edge_maximal_isomorphism(dg1[1], dg2[1])
  set_of_matches = Set()
  for matched_edge in matched_edges:
    start_node1 = matched_edge[0][0]
    end_node1   = matched_edge[0][1]
    start_node2 = matched_edge[1][0]
    end_node2   = matched_edge[1][1]
    set_of_matches.add((start_node1, end_node1, start_node2, end_node2))
    
  if print_matches:
    for start_node1, end_node1, start_node2, end_node2 in set_of_matches:
      break
    for start_node1, end_node1, start_node2, end_node2 in set_of_matches:
      print start_node1, '-->', end_node1, '===>', start_node2, '-->', end_node2
  return set_of_matches


def all_pair_wise_brute_force(list_of_digraphs):
  number_of_graphs = len(list_of_digraphs)
  matchings = defaultdict(list)
  for i in xrange(number_of_graphs):
    label1, dg1 = list_of_digraphs[i]
    for j in xrange(i+1, number_of_graphs):
      label2, dg2 = list_of_digraphs[j]      
      number_nodes = (min(len(dg1.nodes()) ,len(dg2.nodes())), max(len(dg1.nodes()) ,len(dg2.nodes())))
      start = time.time()
      matched_edges = brute_force_edge_maximal_isomorphism(dg1, dg2)
      matchings[(label1, label2)] = len(matched_edges)
      end_time = time.time() - start
  return matchings

def default_start_tensor(grid_size, d):
  return np.zeros(tuple([grid_size]*d))

def run_hashing_no_pairwise(list_of_fftd_graphs, number_runs, vector_sets):
  dimension_for_vector = list_of_fftd_graphs[0][1].shape[0]
  graph_pairs_difference_in_hash_vals = defaultdict(list)
  for run in range(number_runs):
    set_of_unit_vectors = vector_sets[run]
    hash_value_to_graph_label = locality_sensitive_hashing(list_of_fftd_graphs, set_of_unit_vectors)
    graph_label_to_hash_value = {}
    for key in hash_value_to_graph_label:
      for x in hash_value_to_graph_label[key]:
        graph_label_to_hash_value[x] = key

    for hash_value in hash_value_to_graph_label:
      list_of_graph_labels_in_bin = hash_value_to_graph_label[hash_value]
    for i in range(len(list_of_fftd_graphs)):
      for j in range(i + 1, len(list_of_fftd_graphs)):
        bin1 = str("{0:b}".format(graph_label_to_hash_value[i]))
        bin2 = str("{0:b}".format(graph_label_to_hash_value[j]))
        amount_bits_different = np.abs(len(bin1) - len(bin2))
        for k in range(min(len(bin1), len(bin2))):
          if bin1[k] != bin2[k]:
            amount_bits_different += 1
        graph_pairs_difference_in_hash_vals[(i,j)].append(amount_bits_different)
  return graph_pairs_difference_in_hash_vals

def run_hashing_with_pairwise_fast(list_of_fftd_graphs, number_runs, graph_label_and_delta_to_start_tensor_dicts, list_of_all_deltas, vector_sets):
  dimension_for_vector = list_of_fftd_graphs[0][1].shape[0]
  graph_pairs_difference_in_hash_vals = defaultdict(list)
  for run in range(number_runs):
    set_of_unit_vectors = vector_sets[run]
    hash_value_to_graph_label = locality_sensitive_hashing(list_of_fftd_graphs, set_of_unit_vectors)
    graph_label_to_hash_value = {}
    for key in hash_value_to_graph_label:
      for x in hash_value_to_graph_label[key]:
        graph_label_to_hash_value[x] = key

    for hash_value in hash_value_to_graph_label:
      list_of_same_bins = hash_value_to_graph_label[hash_value]
      number_in_bin = len(list_of_same_bins)
      if number_in_bin > 1:
        for i in range(number_in_bin):
          for j in range(i+1, number_in_bin):
            g1 = graph_label_and_delta_to_start_tensor_dicts[i]
            g2 = graph_label_and_delta_to_start_tensor_dicts[j]         

    for hash_value in hash_value_to_graph_label:
      list_of_graph_labels_in_bin = hash_value_to_graph_label[hash_value]

    for i in range(len(list_of_fftd_graphs)):
      for j in range(i + 1, len(list_of_fftd_graphs)):
        bin1 = str("{0:b}".format(graph_label_to_hash_value[i]))
        bin2 = str("{0:b}".format(graph_label_to_hash_value[j]))
        amount_bits_different = np.abs(len(bin1) - len(bin2))
        for k in range(min(len(bin1), len(bin2))):
          if bin1[k] != bin2[k]:
            amount_bits_different += 1
        graph_pairs_difference_in_hash_vals[(i,j)].append(amount_bits_different)
  return graph_pairs_difference_in_hash_vals


def create_all_delta_to_start_tensors(list_of_digraphs, grid_size):
  print 'Creating delta to start tensor dictionaries...'
  graph_label_and_delta_to_start_tensor_dicts = []        
  for label, dg in list_of_digraphs:
    delta_to_start_tensor = defaultdict(lambda: default_start_tensor(grid_size, d))
    for edge in dg.edges():
      start_node_pos = edge[0]
      end_node_pos = edge[1]
      delta = tuple(np.array(end_node_pos) - np.array(start_node_pos))
      delta_to_start_tensor[delta][start_node_pos] = 1
    graph_label_and_delta_to_start_tensor_dicts.append((label, delta_to_start_tensor))
  set_of_all_deltas = Set()
  for dg in graph_label_and_delta_to_start_tensor_dicts:
    for key in dg[1].keys():
      set_of_all_deltas.add(key)
  list_of_all_deltas =  list(set_of_all_deltas)
    
  return graph_label_and_delta_to_start_tensor_dicts, list_of_all_deltas


if __name__=='__main__':
  if len(sys.argv) < 10:
    print 'usage: <d> <number of graphs> <grid size> <min nodes> <max nodes> <min edge cnt> <max edge cnt> <number cutting planes> <number of times to hash>' 
    print 'Good idea to put max pos in graph to (x^2)-1 for some int x'
    exit(1)

  print 'INFORMATION ON RUN'
  seed = int(time.time())
  np.random.seed(seed)
  print '\tseed is', seed

  d                     = int(sys.argv[1])
  number_of_dgs         = int(sys.argv[2])
  grid_size             = int(sys.argv[3])
  min_nodes             = int(sys.argv[4])
  max_nodes             = int(sys.argv[5])
  min_edge_count        = int(sys.argv[6])
  max_edge_count        = int(sys.argv[7])
  number_cutting_planes = int(sys.argv[8])
  number_of_hashings    = int(sys.argv[9])
  
  print '\td', d                     
  print '\tnumber_of_dgs ', number_of_dgs         
  print '\tgrid size', grid_size             
  print '\tmin_edge_count', min_edge_count        
  print '\tmax_edge_count', max_edge_count        
  print '\tnumber_cutting_planes', number_cutting_planes 
  print '\tnumber_of_hashings', number_of_hashings

  run_hashing_trials     = True
  run_all_pairwise_fast  = True
  run_all_pairwise_brute = False

  ###############################
  #          INIT               #
  ###############################
  print 'Gathering random graphs...'
  # Set of random graphs
  list_of_digraphs = random_set_of_graphs(d, number_of_dgs, grid_size, min_nodes, max_nodes, min_edge_count, max_edge_count)

  # Create tensors
  graph_label_and_delta_to_start_tensor_dicts, list_of_all_deltas = create_all_delta_to_start_tensors(list_of_digraphs, grid_size)


  # Create FFT vectors for hashing
  print 'creating fft vectors...'
  list_of_fftd_graphs = []  
  for dg in graph_label_and_delta_to_start_tensor_dicts:
    delta = list_of_all_deltas[0]
    fft_vec = np.abs(np.fft.rfft(np.ndarray.flatten(dg[1][delta])))
    for delta in list_of_all_deltas[1:]:
      v = np.abs(np.fft.rfft(np.ndarray.flatten(dg[1][delta])))
      fft_vec = np.concatenate((fft_vec, v))
    list_of_fftd_graphs.append((dg[0], fft_vec))

  # seed
  np.random.seed(int(time.time()))

  # Unit vectors 
  dimension_for_vector = list_of_fftd_graphs[0][1].shape[0]
  vector_sets = set_random_unit_vectors(number_of_hashings, number_cutting_planes, dimension_for_vector)

  # Print edges/nodes
  total_edges = 0.0
  cnt = 0.
  for i in range(len(list_of_digraphs)):
    dg = list_of_digraphs[i][1]
    total_edges += float(len(dg.edges()))
    cnt +=1
  print "AVERAGE EDGES", total_edges/cnt
  
  total_nodes = 0.0
  cnt = 0.
  for i in range(len(list_of_digraphs)):
    dg = list_of_digraphs[i][1]
    total_nodes += float(len(dg.nodes()))
    cnt +=1
  print "AVERAGE NODES", total_nodes/cnt

  #######################################################################
  #                         TIME TRIALS                                 #
  #######################################################################

  ###############################
  #  LOCALITY SENSITVE HASHING  #
  ###############################
  if run_hashing_trials:
    print '\n\nRUNNING HASHING TIME TEST'
    t0 = time.time()
    graph_pairs_difference_in_hash_vals = run_hashing_no_pairwise(list_of_fftd_graphs, number_of_hashings, vector_sets)
    graph_pairs_difference_in_hash_vals = run_hashing_with_pairwise_fast(list_of_fftd_graphs, number_of_hashings, graph_label_and_delta_to_start_tensor_dicts, list_of_all_deltas, vector_sets)
    hash_time = time.time() - t0
    print 'hashed into bins', number_of_hashings, ' times took ', time.time() - t0, 'seconds'
    fft_time = 0.0
    l = []
    number_hits = 0
    number_pairs = 0
    number_good_hits = 0
    for key in graph_pairs_difference_in_hash_vals:
      number_pairs += 1
      list_hash_differences = graph_pairs_difference_in_hash_vals[key]
      #if list_hash_differences.count(0) == 0:
      #  continue
      number_hits += 1
      g1_label = key[0]
      g2_label = key[1]
      g1 = graph_label_and_delta_to_start_tensor_dicts[g1_label][1]
      g2 = graph_label_and_delta_to_start_tensor_dicts[g2_label][1]
      s = time.time()
      me = fast_edge_maximal_isomorphism(g1, g2, list_of_all_deltas, grid_size, d)
      fft_time += time.time() - s
      if me > 3:
        number_good_hits += 1
      l.append((len(me),key, graph_pairs_difference_in_hash_vals[key], len(list_of_digraphs[key[0]][1].edges()), len(list_of_digraphs[key[1]][1].edges())))
    l = sorted(l)
    for x in l:
      print x[1], x[2], x[0],  x[3], x[4]
    print 'Total time:  ', (hash_time + fft_time),  '\nTime for FFT:', fft_time, '\nHash time:   ', hash_time , '\nNumber hits:', str(number_hits) + '\\' + str(number_pairs), '\ngood hits', number_good_hits, '\nBad hits', number_hits - number_good_hits

  ############### PAIR-WISE SIMILARITY ###############
  if run_all_pairwise_fast:
    print '\n\nRUNNING PAIR-WISE FAST TIME TESTS'
    start = time.time()
    matchings = all_pair_wise_fast(graph_label_and_delta_to_start_tensor_dicts, list_of_digraphs, grid_size, d)
    number_of_pair_wise_comparisons = int(comb(len(graph_label_and_delta_to_start_tensor_dicts), 2))
    print 'Did', number_of_pair_wise_comparisons, ' FAST pair-wise comparisons in      ', time.time() - start, 'seconds'
    for match in matchings:
      print len(matchings[match])
    print

  if run_all_pairwise_brute:  
    print '\n\nRUNNING PAIR-WISE BRUTE FORCE TIME TESTS'
    start1 = time.time()
    matchings = all_pair_wise_brute_force(list_of_digraphs)
    number_of_pair_wise_comparisons = int(comb(len(list_of_digraphs), 2))
    print 'Did', number_of_pair_wise_comparisons, ' BRUTE FORCE pair-wise comparisons in', time.time() - start1, 'seconds'

