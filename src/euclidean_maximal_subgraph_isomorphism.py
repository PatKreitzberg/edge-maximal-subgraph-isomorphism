import networkx as nx
import numpy as np
import itertools
from scipy.signal import correlate

class Memoized:
  def __init__(self, function):
    self._function = function
    self._cache = {}
  def __call__(self, *args):
    if args not in self._cache:
      # not in the cache: call the function and store the result in
      # the cache
      self._cache[args] = self._function(*args)

    # the result must now be in the cache:
    return self._cache[args]

@Memoized
def get_node_depth(digraph, node):
  depths = [ get_node_depth(digraph,pred)+digraph[pred][node]['weight'] for pred in digraph.predecessors(node) ]
  if len(depths) > 0:
    if not all(np.array(depths) == depths[0]):
      raise Exception('Graph cannot be embedded into 1D euclidean: node ' + str(node) + ' has conflicting depths ' + str(depths))
    return depths[0]
  return 0

def get_node_to_int_code(digraph):
  return { x:get_node_depth(digraph, x) for x in digraph.nodes() }


def euclidean_1d_digraph_to_matrix(digraph):
  node_to_int_code = get_node_to_int_code(digraph)
  max_code = max(node_to_int_code.values())

  result = np.zeros( (max_code+1, max_code+1) )
  for a,b in digraph.edges():
    result[node_to_int_code[a], node_to_int_code[b]] = 1
  return result


def euclidean_digraph_to_adjacency_tensor(digraph):
  dimension = 0
  for node in digraph.nodes:
    dimension = len(node[1])
    break
  if dimension==0:
    exit(1)
  nodes = digraph.nodes
  maximum_size_in_a_dimension = max([(y+1) for x in nodes for y in x[1]])
  maximum_size_in_a_dimension = int(2**np.ceil(np.log2(maximum_size_in_a_dimension)))
  dimensions_for_adj_tensor = [maximum_size_in_a_dimension] * 2 * dimension
  adjacency_tensor = np.zeros(tuple(dimensions_for_adj_tensor))
  for edge in digraph.edges:
    start_node = edge[0]
    end_node = edge[1]
    index_in_adj_tensor = start_node + end_node
    adjacency_tensor[index_in_adj_tensor] = 1
  return adjacency_tensor


def mask_array(tensor):
  # Creates mask array to multiply by the tensor
  # this mask is 1's where we allow the answer to come from
  # and 0 otherwise.  Answer must come from symmetric shifts
  # e.g. (0,-1,0,0) is bad, (0,-1,0,-1) is good.
  mask = np.zeros(tensor.shape)
  iterations = [range(tensor.shape[0])] * (len(tensor.shape)/2)
  for index in  itertools.product(*iterations):
    symmetric_index = index+index
    mask[symmetric_index] = 1
  return mask
 
  #mask = np.zeros(tensor.shape)
  #print tensor.shape
  #for i in xrange(np.max(tensor.shape)):
  #  indices = tuple([i]*len(tensor.shape))
  #  mask[indices] = 1
  #return mask

def fast_edge_maximal_isomorphism(delta_to_start_tensor1, delta_to_start_tensor2, list_of_all_deltas, grid_size, d):
  sum_over_deltas = np.zeros(tuple([2*(grid_size)-1]*d))
  for delta in list_of_all_deltas:
    cc = correlate(delta_to_start_tensor2[delta], delta_to_start_tensor1[delta], method='fft')
    sum_over_deltas += cc

  print 'max value', np.max(sum_over_deltas)
  max_overlap_index = sum_over_deltas.argmax()
  shape_for_shift = delta_to_start_tensor1[list_of_all_deltas[0]].shape
  shift = tuple(np.array(np.unravel_index(max_overlap_index, sum_over_deltas.shape)) - shape_for_shift + 1)

  matched_edges_for_a_and_b = []
  delta_to_start_tensor2_copy = {}
  for delta in list_of_all_deltas:
    delta_to_start_tensor2_copy[delta] = np.array(delta_to_start_tensor2[delta])
    for ax in xrange(delta_to_start_tensor2_copy[delta].ndim):
      delta_to_start_tensor2_copy[delta] = np.roll(delta_to_start_tensor2_copy[delta], -shift[ax], axis=ax)

    b_axes = [slice(0, delta_to_start_tensor2_copy[delta].shape[i]) for i in xrange(delta_to_start_tensor2_copy[delta].ndim) ]
    trimmed_edge_tensor_a = delta_to_start_tensor1[delta][tuple(b_axes)]

    a_axes = [slice(0, delta_to_start_tensor1[delta].shape[i]) for i in xrange(delta_to_start_tensor1[delta].ndim) ]
    trimmed_edge_tensor_b = delta_to_start_tensor2_copy[delta][tuple(a_axes)]

    intersecting_edges_shifted_to_tensor_a = np.array(np.round(trimmed_edge_tensor_a * trimmed_edge_tensor_b), bool)
    indices = list(itertools.product(*[ range(0,end) for end in intersecting_edges_shifted_to_tensor_a.shape ]))
    
    for a_index, edge_matches in zip(indices, intersecting_edges_shifted_to_tensor_a.flatten()):

      if edge_matches:
        b_index = tuple(np.array(a_index,int)+shift)
        edge_a = (a_index, tuple(np.array(a_index) + np.array(delta)))
        edge_b = (b_index, tuple(np.array(b_index) + np.array(delta)))        
        matched_edges_for_a_and_b.append( (edge_a, edge_b) )

  return matched_edges_for_a_and_b

def iso_edge_check(dg1, edge1, dg2, edge2):
  a_coord = edge1[0][1]
  b_coord = edge1[1][1]
  vec1 = np.array(a_coord) - np.array(b_coord)
  a2_coord = edge2[0][1]
  b2_coord = edge2[1][1]
  vec2 = np.array(a2_coord) - np.array(b2_coord)
  if a_coord == b_coord or a2_coord == b2_coord:
    return False
  return np.array_equal(vec1, vec2)


def shift(a, a2):
  va = np.array(a[1])
  va2 = np.array(a2[1])
  return va2 - va


def return_shifted_edge(edge, shift_to_match):
  return tuple(edge[0] + shift_to_match) + tuple(edge[1] + shift_to_match)

def same_delta(edge, edge2):
  delta  = np.array(edge[0]) - np.array(edge[1])
  delta2  = np.array(edge2[0]) - np.array(edge2[1])
  return np.array_equal(delta, delta2)

def brute_force_edge_maximal_isomorphism(dg, dg2):
  dg_nodes = list(dg.nodes())
  dg2_nodes = list(dg2.nodes())

  if len(dg_nodes) > len(dg2_nodes):
    return brute_force_edge_maximal_isomorphism(dg2, dg)
  best_num_matching_edges = -np.inf
  best_edge_matches = None

  # try all orderings of nodes in dg2:
  for ordered_dg2_nodes in itertools.permutations(dg2_nodes, min(len(dg_nodes), len(dg2_nodes))):
    translate_to_dg2 = dict(zip(dg_nodes, ordered_dg2_nodes))
    dg_mapped = nx.DiGraph()

    edge_matches = []
    for a,b in dg.edges():
      a2, b2 = translate_to_dg2[a], translate_to_dg2[b]
      if b2 in dg2[a2]:
        if same_delta( (a, b), (a2, b2) ):
          dg_mapped.add_edge(a,b)
          edge_matches.append( ((a,b), (a2,b2)) )

    num_matching_edges = len(dg_mapped.edges())
    if num_matching_edges > best_num_matching_edges:
      best_num_matching_edges = num_matching_edges
      best_edge_matches = edge_matches
  print 'MATCHES', best_num_matching_edges
  return best_edge_matches


def n_dim_to_edge_tensor(dg):
  # Finding dimensions of dg and adj. tensor so this can be general
  # for any dimensional graph
  nodes = list(dg.nodes())
  dim = len(nodes[0]) - 1
  maximum_size_in_a_dimension = max([y for x in nodes for y in x[1:]])
  maximum_size_in_a_dimension = int(2**np.ceil(np.log2(maximum_size_in_a_dimension)))
  dimensions_for_adj_tensor = [maximum_size_in_a_dimension] * 2 * dim
  adjacency_tensor = np.zeros(tuple(dimensions_for_adj_tensor))

  edges = list(dg.edges())
  # edges is in the style [e0, e1,...]
  # where e_i is tuple(node1, node2)
  # and node1 is tuple(name as str, coord_1, coord_2,..., coord_n)
  for edge in edges:
    node1_coordinates = edge[0]
    node2_coordinates = edge[1]
    coordinate_in_adj_tensor = node1_coordinates + node2_coordinates
    adjacency_tensor[coordinate_in_adj_tensor] = 1
  return adjacency_tensor

def dist(a,b):
  return np.linalg.norm([x-y for x,y in zip(b[1:], a[1:])])
  
def create_two_3_dim_graphs():
  # nx allows any hashable object as node
  # so we can do tuple with name then the dimensions  
  dg = nx.DiGraph()
  a = ('a', 5,5,5)
  b = ('b', 5,6,5)
  c = ('c', 6,6,6)
  d = ('d', 6,7,6)
  dg.add_edge(a, b, weight=dist(a, b))
  dg.add_edge(b, c, weight=dist(b, c))
  dg.add_edge(b, d, weight=dist(b, d))
  dg.add_edge(c, d, weight=dist(c, d))

  dg2 = nx.DiGraph()
  A = ('A', 4,4,4)
  B = ('B', 4,5,4)
  C = ('C', 5,5,5)
  D = ('D', 5,6,5)
  dg2.add_edge(A, B, weight=dist(A, B))
  dg2.add_edge(B, C, weight=dist(B, C))
  dg2.add_edge(B, D, weight=dist(B, D))
  dg2.add_edge(C, D, weight=dist(C, D))
  E = ('E', 1,0,1)
  F = ('F', 0,1,4)
  G = ('G', 1,1,2)
  H = ('H', 3,4,5)
  
  dg2.add_edge(E,F, weight=dist(E,F))
  dg2.add_edge(F,G, weight=dist(F,G))
  dg2.add_edge(F,H, weight=dist(F,H))
  dg2.add_edge(G,H, weight=dist(G,H))
  return dg, dg2


def dim_3_brute_force_edge_maximal_isomorphism(dg, dg2):
  dg_nodes = list(dg.nodes())
  dg2_nodes= list(dg2.nodes())

  best_num_matching_edges = -np.inf
  best_edge_matches = None

  # try all orderings of nodes in dg2:
  for ordered_dg2_nodes in itertools.permutations(dg2_nodes, min(len(dg_nodes), len(dg2_nodes))):
    translate_to_dg2 = dict(zip(dg_nodes, ordered_dg2_nodes))
    dg_mapped = nx.DiGraph()
    
    edge_matches = []
    for a,b in dg.edges():
      a2, b2 = translate_to_dg2[a], translate_to_dg2[b]
      if b2 in dg2[a2]:
        v1 = (b[1]-a[1], b[2]-a[3], b[3]-a[3])
        v2 = (b2[1]-a2[1], b2[2]-a2[3], b2[3]-a2[3])
        if v1==v2:
          dg_mapped.add_edge(a,b)
          edge_matches.append( ((a,b), (a2,b2)) )
    
    num_matching_edges = len(dg_mapped.edges())
    if num_matching_edges > best_num_matching_edges:
      best_num_matching_edges = num_matching_edges
      best_edge_matches = edge_matches

  return best_edge_matches

def dim_3_find_max_subgraph_matching():
  dim_3_dg1, dim_3_dg2 = create_two_3_dim_graphs()
  dim_3_adj_tensor1 = n_dim_to_edge_tensor(dim_3_dg1)
  dim_3_adj_tensor2 = n_dim_to_edge_tensor(dim_3_dg2)
  print
  print
  print 'THREE DIMENSIONAL EUCLIDEAN GRAPH'
  matched_edges = brute_force_edge_maximal_isomorphism(dim_3_dg1, dim_3_dg2)
  print 'BRUTE FORCE'
  print '-------------'
  print 'Found ' , len(matched_edges), ' edge matches:'  
  for matched_edge in matched_edges:
    dg1_nodes = matched_edge[0]
    dg2_nodes = matched_edge[1]
    dg1_a = dg1_nodes[0][1:]
    dg1_b = dg1_nodes[1][1:]
    dg2_a = dg2_nodes[0][1:]
    dg2_b = dg2_nodes[1][1:]
    dg1_a_node = None
    dg1_b_node = None
    dg2_a_node = None
    dg2_b_node = None    
    for node in dim_3_dg1.nodes():
      if node[1:] == dg1_a:
        dg1_a_node = node
      elif node[1:] == dg1_b:
        dg1_b_node = node
        
    for node in dim_3_dg2.nodes():
      if node[1:] == dg2_a:
        dg2_a_node = node
      elif node[1:] == dg2_b:
        dg2_b_node = node
  
    print dg1_a_node[0], '-->', dg1_b_node[0], '\tmatches\t', dg2_a_node[0], '-->', dg2_b_node[0]

  matched_edges = fast_edge_maximal_isomorphism(dim_3_adj_tensor1, dim_3_adj_tensor2)  
  print 'FAST VERSION'
  print '-------------'
  print 'Found ' , len(matched_edges), ' edge matches:'
  for matched_edge in matched_edges:
    dg1_nodes = matched_edge[0]
    dg2_nodes = matched_edge[1]
    dg1_a = dg1_nodes[:3]
    dg1_b = dg1_nodes[3:]
    dg2_a = dg2_nodes[:3]
    dg2_b = dg2_nodes[3:]    
    dg1_a_node = None
    dg1_b_node = None
    dg2_a_node = None
    dg2_b_node = None    
    for node in dim_3_dg1.nodes():
      if node[1:] == dg1_a:
        dg1_a_node = node
      elif node[1:] == dg1_b:
        dg1_b_node = node
        
    for node in dim_3_dg2.nodes():
      if node[1:] == dg2_a:
        dg2_a_node = node
      elif node[1:] == dg2_b:
        dg2_b_node = node
  
    print dg1_a_node[0], '-->', dg1_b_node[0], '\tmatches\t', dg2_a_node[0], '-->', dg2_b_node[0]


def original_1d_graph():
  dg = nx.DiGraph()
  dg.add_edge('a','b',weight=1)
  dg.add_edge('a','c',weight=2)
  dg.add_edge('a','d',weight=4)
  dg.add_edge('b','d',weight=3)
  
  dg2 = nx.DiGraph()
  dg2.add_edge('X','A',weight=4)
  dg2.add_edge('X','Y',weight=2)
  dg2.add_edge('Y','B',weight=3)
  dg2.add_edge('A','B',weight=1)
  dg2.add_edge('A','C',weight=2)
  dg2.add_edge('A','D',weight=4)
  dg2.add_edge('B','D',weight=3)
  dg2.add_edge('D','E',weight=6)
  dg2.add_edge('C','E',weight=8)
  return dg, dg2

def reduce_function(x_int, y_int):
  #42, 2013 -> 2 0 0 0 1 4 3 2
  #42            0   0   4   2
  #    2013    2   0   1   3
  x = str(x_int)
  y = str(y_int)
  if len(x) < len(y):
    x = '0'* (len(y)-len(x)) + x
  elif len(y) < len(x):
    y = '0'* (len(x)-len(y)) + y
  
  z = ''
  for i in range(len(x)):
    z += y[i] + x[i]
  return int(z)
    
def dim_2d_reduced_to_1d():
  dg = nx.DiGraph()
  a = ('a', reduce_function(1,1))
  b = ('b', reduce_function(1,2))
  c = ('c', reduce_function(2,3))
  d = ('d', reduce_function(3,2))
  
  dg.add_edge('a','b',weight=int(np.fabs(a[1] - b[1]) ))
  dg.add_edge('b','c',weight=int(np.fabs(b[1] - c[1]) ))
  dg.add_edge('c','d',weight=int(np.fabs(c[1] - d[1]) ))
  
  dg2 = nx.DiGraph()
  A = ('A', reduce_function(1,1))
  B = ('B', reduce_function(1,2))
  C = ('C', reduce_function(2,3))
  D = ('D', reduce_function(3,2))  
  dg2.add_edge('A','B',weight=int(np.fabs(A[1] - B[1]) ))
  dg2.add_edge('B','C',weight=int(np.fabs(B[1] - C[1]) ))
  dg2.add_edge('C','D',weight=int(np.fabs(C[1] - D[1]) ))
  return dg, dg2
  
if __name__=='__main__':
  # original
  #dg, dg2 = original_1d_graph()
  dg, dg2 = dim_2d_reduced_to_1d()
  
  print 'BRUTE FORCE'
  print '-------------'
  matched_edges = brute_force_edge_maximal_isomorphism(dg, dg2)
  print 'Found', len(matched_edges), 'edge matches:'
  for (a_source,a_dest),(b_source,b_dest) in matched_edges:
    print a_source, '-->', a_dest, '\tmatches\t', b_source, '-->', b_dest
  print
    
  print 'FAST VERSION'
  print '-------------'
  print 'Using the following embeddings:'
  edge_mat = euclidean_1d_digraph_to_matrix(dg)
  edge_mat2 = euclidean_1d_digraph_to_matrix(dg2)
  #print edge_mat
  #print edge_mat2
  matched_edges = fast_edge_maximal_isomorphism(edge_mat, edge_mat2)
  print 'Found', len(matched_edges), 'edge matches:'

  # translate integer codes back to node keys:
  a_int_code_to_node = {v:k for k,v in get_node_to_int_code(dg).items()}
  b_int_code_to_node = {v:k for k,v in get_node_to_int_code(dg2).items()}

  for (a_source_code,a_dest_code),(b_source_code,b_dest_code) in matched_edges:
    a_source = a_int_code_to_node[a_source_code]
    a_dest = a_int_code_to_node[a_dest_code]
    b_source = b_int_code_to_node[b_source_code]
    b_dest = b_int_code_to_node[b_dest_code]
    print a_source, '-->', a_dest, '\tmatches\t', b_source, '-->', b_dest
  

  dim_3_find_max_subgraph_matching()
  
