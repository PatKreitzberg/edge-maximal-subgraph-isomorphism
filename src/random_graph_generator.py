import networkx as nx
import numpy as np
import matplotlib.pyplot as P
import itertools
from collections import defaultdict
from scipy.signal import correlate
from scipy.misc import comb
from sets import Set
from random import choice
import sys

def random_edge(dg, grid_size):
  node1 = choice(list(dg.nodes))
  node2 = choice(list(dg.nodes))
  while dg.has_edge(node1, node2) or dg.has_edge(node2, node1) or node1==node2:
    node1 = choice(list(dg.nodes))
    node2 = choice(list(dg.nodes))
  return node1, node2

def random_isomorphic_shift(list_of_digraphs, index):
  edge_donor_graph = np.random.randint(len(list_of_digraphs))
  while edge_donor_graph == index:
    edge_donor_graph = np.random.randint(len(list_of_digraphs))
  edge = choice(list(list_of_digraphs[edge_donor_graph][1].edges))
  return np.array(edge[1]) - np.array(edge[0])

def add_edge_with_shift(dg, shift, grid_size):
  node1 = None
  node2 = None
  reroll = True
  rolls = 0
  while reroll:
    reroll = False
    node1 = choice(list(dg.nodes))
    node2 = np.array(node1) + shift    
    for i in range(len(node2)):
      if node2[i] >= grid_size or node2[i] < 0:
        reroll = True
    if rolls > 10:
      return False

  dg.add_edge(tuple(node1), tuple(node2))
  return True
  
def random_set_of_graphs(dimension, number_of_dg, grid_size, number_nodes_low, number_nodes_high, number_edges_low, number_edges_high):
  max_position = grid_size - 1
  label_and_networkx_graphs = []
  for i in xrange(number_of_dg):
    dg = nx.DiGraph()
    number_nodes = np.random.randint(number_nodes_low, high=number_nodes_high)
    number_edges = min(comb(number_nodes, 2), np.random.randint(number_edges_low, high=number_edges_high))    
    positions = []
    for j in xrange(number_nodes):
      position = tuple(np.random.randint(max_position, size=2))
      while dg.has_node(position):
        position = np.random.randint(max_position, size=2)
      dg.add_node(tuple(position))
      positions.append(position)

    while len(dg.edges()) < number_edges:
      node1, node2 = random_edge(dg, grid_size)
      if i > 0:
        how_to_add = np.random.random()
        if how_to_add < 10.99:
          shift = random_isomorphic_shift(label_and_networkx_graphs, i)
          if not add_edge_with_shift(dg, shift, grid_size):
            dg.add_edge(node1, node2)
        else:
          dg.add_edge(node1, node2)
      else:
        dg.add_edge(node1, node2)
      
    label_and_networkx_graphs.append((i,dg))
  return label_and_networkx_graphs

def add_edge_from_one_dg_to_another(dg1, dg2):
  edge_to_copy = choice(list(dg1.edges))
  while dg2.has_edge(edge_to_copy):
    edge_to_copy = choice(list(dg1.edges))
  dg2.add_edge(edge_to_copy)
  return 1

def add_edge_to_both(dg1, dg2, max_position):
  new_edge = (np.random.randint(max_position), np.random.randint(max_position))
  while dg1.has_edge(new_edge) or dg2.has_edge(new_edge) or new_edge[0] == new_edge[1]:
    new_edge = (np.random.randint(max_position), np.random.randint(max_position))
  dg1.add_edge(new_edge)
  dg2.add_edge(new_edge)
  return 1

def add_aligned_edges(dg1, dg2, max_position, number_edges_to_add=None):
  edges_added = 0
  for e0, e1 in zip(dg1.edges(), dg2.edges()):
    if e0 == e1:
      number_edges_to_add -= 1
  while edges_added < number_edges_to_add:
    r = np.random.randint(3)
    edge_from = None
    edges_to = None
    if r == 0:
      number_edges_to_add += add_edge_from_one_dg_to_another(dg1, dg2)
    elif r == 1:
      number_edges_to_add += add_edge_from_one_dg_to_another(dg2, dg1)
    else:
      number_edges_to_add += add_edge_to_both(dg1, dg2)


if __name__=='__main__':
  if len(sys.argv) < 7:
    print 'usage: <d> <number of graphs> <max pos in graph> <min graph size> <max graph size> <lower bound on num aligned edges>'
    print 'Good idea to put max pos in graph to (x^2)-1 for some int x'
    exit(1)
    
  d                = int(sys.argv[1])
  number_of_dgs    = int(sys.argv[2])
  max_value        = int(sys.argv[3])
  min_graph_size   = int(sys.argv[4])
  max_graph_size   = int(sys.argv[5])
  lower_bound_hits = int(sys.argv[6])

  
  # get a set of random graphs
  list_of_digraphs = random_set_of_graphs(d, number_of_dgs, max_value, min_graph_size, max_graph_size)
  add_aligned_edges(list_of_digraphs[0][1], list_of_digraphs[1][1], lower_bound_hits)
  for dg1_and_name, dg2_and_name in itertools.combinations(list_of_digraphs, 2):
    if dg1_and_name[0] != dg2_and_name[0]:
      dg1 = dg1_and_name[1]
      dg2 = dg2_and_name[1]
      #print 'DiGraph 1 nodes:', dg1.nodes()
      #print 'DiGraph 2 nodes:', dg2.nodes()
      print 'number of edges', len(dg1.edges()), len(dg2.edges())
      dir_dg1 = []
      dir_dg2 = []      
      #print 'DiGraph 1 edges:'
      for edge in dg1.edges():
        #print '\t', edge[0][0], '-->', edge[1][0] , '\t', edge[0][1], '-->', edge[1][1], '\t', np.array(edge[1][1]) - np.array(edge[0][1])
        dir_dg1.append((np.array(edge[1][1]) - np.array(edge[0][1]), edge))
      
      print
      #print 'DiGraph 2 edges:'
      for edge in dg2.edges():
        #print '\t', edge[0][0], '-->', edge[1][0] , '\t', edge[0][1], '-->', edge[1][1], '\t', np.array(edge[1][1]) - np.array(edge[0][1])
        dir_dg2.append((np.array(edge[1][1]) - np.array(edge[0][1]), edge))
            
        
      d = len(list(dg1.nodes)[0][1])
      print '========= DiGraphs ', dg1_and_name[0], ' and ', dg2_and_name[0], '========='
      dg1_adj_tensor = euclidean_digraph_to_adjacency_tensor(dg1)
      dg2_adj_tensor = euclidean_digraph_to_adjacency_tensor(dg2)      
      print 'Fast version'
      matched_edges = fast_edge_maximal_isomorphism(dg1_adj_tensor, dg2_adj_tensor)
      if len(matched_edges) > 0:
        print_matched_edges(matched_edges, dg1, dg2, d)
      else:
        print '\tno matches'

      print '\nBrute Force'
      matched_edges = brute_force_edge_maximal_isomorphism(dg1, dg2)
      if matched_edges != None:
        print_matched_edges_brute_force(matched_edges, dg1, dg2, d)
      else:
        print '\tno matches'
